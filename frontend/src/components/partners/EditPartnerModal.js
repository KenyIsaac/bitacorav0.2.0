import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import SettlementSelectOptions from '../settlements/SettlementSelectOptions';
import CompanyformSelectOptions from '../companyforms/CompanyformSelectOptions';
import PropTypes from 'prop-types';
import { updatePartner } from '../../actions/partnerActions';

import M from 'materialize-css/dist/js/materialize.min.js';

const EditPartnerModal = ({ current, updatePartner }) => {
    const [name, setName] = useState('');
    const [companyform_id, setCompanyform] = useState('');
    const [settlement_id, setSettlement] = useState('');
    const [tax_number, setTax_number] = useState('');
    const [company_reg_number, setCompany_reg_number] = useState('');
    const [address, setAddress] = useState('');
    const [phone_number, setPhone_number] = useState('');
    const [bank_account_number, setBank_account_number] = useState('');
    const [comment, setComment] = useState('');

    useEffect(() => {
        if (current) {
            setName(current.name);
            setCompanyform(current.companyform_id);
            setSettlement(current.settlement_id);
            setTax_number(current.tax_number);
            setCompany_reg_number(current.company_reg_number);
            setAddress(current.address);
            setPhone_number(current.phone_number);
            setBank_account_number(current.bank_account_number);
            setComment(current.comment);
        }
      }, [current]);

    const onSubmit =() => {
        if(name === '' || companyform_id ==='' || settlement_id==='') {
            M.toast({ html: 'El nombre del cliente es requerido'})
        } else{

            const updPartner = {
                id: current.id,
                name,
                companyform_id,
                tax_number,
                company_reg_number,
                settlement_id,
                address,
                phone_number,
                bank_account_number,
                comment
              };
        
              updatePartner(updPartner);
              M.toast({ html: 'Usuario actualizado' });
              window.location.reload(false)

            //Clear Fields
            setName('');
            setCompanyform('');
            setTax_number('');
            setCompany_reg_number('');
            setSettlement('');
            setAddress('');
            setPhone_number('');
            setBank_account_number('');
            setComment('');
        }     
    }

    return (
    <div id="edit-partner-modal" className='modal'>
        <div className='modal-content'>
            <h4 className='center'>Bitácora v0.2.0</h4>
            <h5 className='center'>Primer Momento</h5>

{/* Tipo de documento */}
            <div className='row'> 
                <select name='companyform' value={companyform_id} className='browser-default' onChange={e => setCompanyform(e.target.value)}>                
                <option value="" disabled selected>Tipo de documento</option>
                <CompanyformSelectOptions />
                </select>
                <label>Seleccione el tipo de Documento</label>
            </div>

{/* Número de documento */}
            <div className='row'>
                <div className='row'>
                    <input type="number" name='tax_number' value={tax_number} onChange={e => setTax_number(e.target.value)}>
                    </input>
                    <label htmlFor='tax_number' className='active'>
                    Número de documento
                    </label>              
                </div>
            </div>

{/* Nombre cliente */}
            <div className='row'>
                <div className='row'>
                    <input type="text" name='name' value={name} onChange={e => setName(e.target.value)}>
                    </input>
                    <label htmlFor='name' className='active'>
                        Nombre cliente
                    </label>              
                </div>
            </div>

{/* Número contacto */}
            <div className='row'>
                <div className='row'>
                    <input type="number" name='company_reg_number' value={company_reg_number} onChange={e => setCompany_reg_number(e.target.value)}>
                    </input>
                    <label htmlFor='company_reg_number' className='active'>
                    Número de Contacto
                    </label>              
                </div>
            </div>

{/* Pagaduría */}
            <div className='row'> 
                <select name='settlement' value={settlement_id} className='browser-default' onChange={e => setSettlement(e.target.value)}>                
                <option value="" disabled selected>Pagaduría</option>
                 <SettlementSelectOptions />
                </select>
                <label>Pagaduría</label>
            </div>

{/* Monto Solicitado */}
            <div className='row'>
                <div className='row'>
                    <input type="number" name='address' value={address} onChange={e => setAddress(e.target.value)}>
                    </input>
                    <label htmlFor='address' className='active'>
                    Monto Solicitado
                    </label>              
                </div>
            </div>

{/* Monto Aprobado */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="number" name='address2' value={address2} onChange={e => setAddress(e.target.value)}>
                    </input>
                    <label htmlFor='address2' className='active'>
                    Monto Aprobado
                    </label>              
                </div>
            </div> */}

{/* Plazo */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="number" name='address3' value={address3} onChange={e => setAddress(e.target.value)}>
                    </input>
                    <label htmlFor='address3' className='active'>
                    Monto Aprobado
                    </label>              
                </div>
            </div> */}

{/* Modalidad */}
            {/* <div className='row'> 
                <select name='settlements2' value={settlements2_id} className='browser-default' onChange={e => setSettlement(e.target.value)}>                
                <option value="" disabled selected>Modalidad</option>
                 <SettlementSelectOptions />
                </select>
                <label>Modalidad</label>
            </div> */}

{/* Cédula de Ejecutivo */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="number" name='tax_number2' value={tax_number2} onChange={e => setTax_number(e.target.value)}>
                    </input>
                    <label htmlFor='tax_number2' className='active'>
                    Cédula de Ejecutivo
                    </label>              
                </div>
            </div> */}

{/* Nombre Ejecutivo */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="text" name='name2' value={name2} onChange={e => setName(e.target.value)}>
                    </input>
                    <label htmlFor='name2' className='active'>
                        Nombre Ejecutivo
                    </label>              
                </div>
            </div> */}

{/* Nombre coordinador */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="text" name='name3' value={name3} onChange={e => setName(e.target.value)}>
                    </input>
                    <label htmlFor='name3' className='active'>
                        Nombre Coordinador
                    </label>              
                </div>
            </div> */}

{/* Nombre Director */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="text" name='name4' value={name4} onChange={e => setName(e.target.value)}>
                    </input>
                    <label htmlFor='name4' className='active'>
                        Nombre Director
                    </label>              
                </div>
            </div> */}

            <h5 className='center'>Segundo Momento</h5>

{/* Banca Seguros */}
            {/* <div className='row'> 
                <select name='companyform2' value={companyform2_id} className='browser-default' onChange={e => setCompanyform(e.target.value)}>                
                <option value="" disabled selected>Banca Seguro</option>
                <CompanyformSelectOptions />
                </select>
                <label>Banca Seguro</label>
            </div>             */}

{/* Tasa */}
            <div className='row'>
                <div className='row'>
                    <input type="number" name='phone_number' value={phone_number} onChange={e => setPhone_number(e.target.value)}>
                    </input>
                    <label htmlFor='phone_number' className='active'>
                    Tasa
                    </label>              
                </div>
            </div>

{/* Código Oficina de Desembolso */}
            {/* <div className='row'> 
                <select name='settlement3' value={settlement3_id} className='browser-default' onChange={e => setSettlement(e.target.value)}>                
                <option value="" disabled selected>Código Oficina de Desembolso</option>
                 <SettlementSelectOptions />
                </select>
                <label>Código Oficina de Desembolso</label>
            </div> */}

{/* Oficina de Desembolso */}
            {/* <div className='row'> 
                <select name='settlement4' value={settlement4_id} className='browser-default' onChange={e => setSettlement(e.target.value)}>                
                <option value="" disabled selected>Oficina de Desembolso</option>
                 <SettlementSelectOptions />
                </select>
                <label>Oficina de Desembolso</label>
            </div> */}

{/* Ciudad */}
            {/* <div className='row'> 
                <select name='settlement5' value={settlement5_id} className='browser-default' onChange={e => setSettlement(e.target.value)}>                
                <option value="" disabled selected>Ciudad</option>
                 <SettlementSelectOptions />
                </select>
                <label>Ciudad</label>
            </div> */}

{/* Zona */}
            {/* <div className='row'> 
                <select name='settlement6' value={settlement6_id} className='browser-default' onChange={e => setSettlement(e.target.value)}>                
                <option value="" disabled selected>Zona</option>
                 <SettlementSelectOptions />
                </select>
                <label>Zona</label>
            </div> */}

{/* Estado de Radicación */}
            {/* <div className='row'> 
                <select name='settlement7' value={settlement7_id} className='browser-default' onChange={e => setSettlement(e.target.value)}>                
                <option value="" disabled selected>Estado de Radicación</option>
                 <SettlementSelectOptions />
                </select>
                <label>Estado de Radicación</label>
            </div> */}

{/* Tipo de proceso */}
            {/* <div className='row'> 
                <select name='settlement8' value={settlement8_id} className='browser-default' onChange={e => setSettlement(e.target.value)}>                
                <option value="" disabled selected>Tipo de Proceso</option>
                 <SettlementSelectOptions />
                </select>
                <label>Tipo de proceso</label>
            </div> */}

{/* Periodo */}
            <div className='row'>
                <div className='row'>
                    <input type="date" name='bank_account_number' value={bank_account_number} onChange={e => setBank_account_number(e.target.value)}>
                    </input>
                    <label htmlFor='bank_account_number' className='active'>
                    Periodo
                    </label>              
                </div>
            </div>

{/* Fecha de radicación */}
{/* <div className='row'>
                <div className='row'>
                    <input type="date" name='bank_account_number2' value={bank_account_number2} onChange={e => setBank_account_number(e.target.value)}>
                    </input>
                    <label htmlFor='bank_account_number' className='active'>
                    Fecha de radicación
                    </label>              
                </div>
            </div> */}


{/* Compra afecta desprendible */}
            {/* <div className='row'> 
                <select name='settlement8' value={settlement8_id} className='browser-default' onChange={e => setSettlement(e.target.value)}>                
                <option value="" disabled selected>Compra afecta desprendible</option>
                 <SettlementSelectOptions />
                </select>
                <label>Compra afecta desprendible</label>
            </div> */}

{/* Entidad a comprar */}
            {/* <div className='row'> 
                <select name='settlement10' value={settlement10_id} className='browser-default' onChange={e => setSettlement(e.target.value)}>                
                <option value="" disabled selected>Entidad a comprar</option>
                 <SettlementSelectOptions />
                </select>
                <label>Entidad a comprar</label>
            </div> */}

{/* Tipo de producto */}
            {/* <div className='row'> 
                <select name='settlement11' value={settlement11_id} className='browser-default' onChange={e => setSettlement(e.target.value)}>                
                <option value="" disabled selected>Tipo de Producto</option>
                    <SettlementSelectOptions />
                </select>
                <label>Tipo de Producto</label>
            </div> */}

{/* Número de obligación de compra*/}
            {/* <div className='row'>
                <div className='row'>
                    <input type="number" name='company_reg_number2' value={company_reg_number2} onChange={e => setCompany_reg_number(e.target.value)}>
                    </input>
                    <label htmlFor='company_reg_number2' className='active'>
                    Número de obligación de compra
                    </label>              
                </div>
            </div> */}


<h5 className='center'>Tercer Momento</h5>

{/* Fecha de Devolución Negativo */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="date" name='bank_account_number3' value={bank_account_number2} onChange={e => setBank_account_number(e.target.value)}>
                    </input>
                    <label htmlFor='bank_account_number' className='active'>
                    Fecha de Devolución Negativo
                    </label>              
                </div>
            </div> */}

{/* Fecha No viable */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="date" name='bank_account_number4' value={bank_account_number4} onChange={e => setBank_account_number(e.target.value)}>
                    </input>
                    <label htmlFor='bank_account_number' className='active'>
                    Fecha No viable
                    </label>              
                </div>
            </div> */}

{/* Fecha de desembolso */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="date" name='bank_account_number5' value={bank_account_number5} onChange={e => setBank_account_number(e.target.value)}>
                    </input>
                    <label htmlFor='bank_account_number' className='active'>
                    Fecha de desembolso
                    </label>              
                </div>
            </div> */}

{/* Número causal*/}
            {/* <div className='row'>
                <div className='row'>
                    <input type="number" name='company_reg_number3' value={company_reg_number3} onChange={e => setCompany_reg_number(e.target.value)}>
                    </input>
                    <label htmlFor='company_reg_number2' className='active'>
                    Número causal
                    </label>              
                </div>
            </div> */}

{/* Causal */}
            <div className='row'>
                <div className='row'>
                    <input type="text" name='comment' value={comment} onChange={e => setComment(e.target.value)}>
                    </input>
                    <label htmlFor='comment' className='active'>
                    Causal
                    </label>              
                </div>
            </div>

{/* Observación devolución negativo */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="text" name='comment2' value={comment2} onChange={e => setComment(e.target.value)}>
                    </input>
                    <label htmlFor='comment' className='active'>
                    Observación devolución negativo
                    </label>              
                </div>
            </div> */}

{/* Paz y Salvo */}
            {/* <div className='row'> 
                <select name='companyform3' value={companyform3_id} className='browser-default' onChange={e => setCompanyform(e.target.value)}>                
                <option value="" disabled selected>Paz y Salvo</option>
                <CompanyformSelectOptions />
                </select>
                <label>Paz y Salvo</label>
            </div> */}

{/* Soporte de pago */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="text" name='comment3' value={comment3} onChange={e => setComment(e.target.value)}>
                    </input>
                    <label htmlFor='comment' className='active'>
                    Soporte de pago
                    </label>              
                </div>
            </div> */}

{/* Observaciones Legales */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="text" name='comment4' value={comment4} onChange={e => setComment(e.target.value)}>
                    </input>
                    <label htmlFor='comment' className='active'>
                    Observaciones Legales
                    </label>              
                </div>
            </div> */}

{/* Segmentación */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="text" name='comment5' value={comment5} onChange={e => setComment(e.target.value)}>
                    </input>
                    <label htmlFor='comment' className='active'>
                    Segmentación
                    </label>              
                </div>
            </div> */}

{/* Tipo de contrato */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="text" name='comment6' value={comment6} onChange={e => setComment(e.target.value)}>
                    </input>
                    <label htmlFor='comment' className='active'>
                    Tipo de contrato
                    </label>              
                </div>
            </div> */}

{/* Estado de Contrato */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="text" name='comment7' value={comment7} onChange={e => setComment(e.target.value)}>
                    </input>
                    <label htmlFor='comment' className='active'>
                    Estado de Contrato
                    </label>              
                </div>
            </div> */}

{/* Cargo */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="text" name='comment8' value={comment8} onChange={e => setComment(e.target.value)}>
                    </input>
                    <label htmlFor='comment8' className='active'>
                    Cargo
                    </label>              
                </div>
            </div> */}

{/* Fecha de ingreso */}
            {/* <div className='row'>
                <div className='row'>
                    <input type="date" name='bank_account_number6' value={bank_account_number6} onChange={e => setBank_account_number(e.target.value)}>
                    </input>
                    <label htmlFor='bank_account_number' className='active'>
                    Fecha de Ingreso
                    </label>              
                </div>
            </div> */}

        </div>
        <div className='moda-footer'>
        <div className='row'>
        <div className="col s3">
        <button href='#!' onClick={onSubmit} className='modal-close waves-effect blue waves-light btn'
        >Enviar
        <i className="material-icons right">send</i>
        </button>
        </div>
        <div className="col s5">
        <button href='#add-settlement-modal' onClick={onSubmit} className='modal-close waves-effect blue waves-light btn modal-trigger'
        >Agregar detalle
        <i className="material-icons right">location_on</i>
        </button>
        </div>
        </div>
        </div>
    </div>
    )
}

EditPartnerModal.propTypes = {
    current: PropTypes.object,
    updatePartner: PropTypes.func.isRequired
  };
  
  const mapStateToProps = state => ({
    current: state.partner.current
  });
  
  export default connect(mapStateToProps,{ updatePartner })(EditPartnerModal);