import React from "react";

const Preloader = () => {
    return (
    <div className="progress blue lighten-3">
        <div className="indeterminate yellow"></div>
    </div>
    )
};

export default Preloader