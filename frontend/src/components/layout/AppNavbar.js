import React from 'react';

const AppNavbar = ({ }) => {
      
     return (
    <nav>
        <div className="nav-wrapper blue">
            <a href="#" className="brand-logo center">
                <i className="material-icons ">assignment_ind</i>
                Bitácora v0.2.0
            </a>
        </div>
      </nav>
    )
}
  
export default AppNavbar;